/**
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file rasterizerAttrib.cxx
 * @author Matthieu Jacquemet
 * @date 2022-07-18
 */

#include "rasterizerAttrib.h"
#include "graphicsStateGuardianBase.h"
#include "dcast.h"
#include "bamReader.h"
#include "bamWriter.h"
#include "datagram.h"
#include "datagramIterator.h"

TypeHandle RasterizerAttrib::_type_handle;
int RasterizerAttrib::_attrib_slot;

/**
 * Constructs a new RasterizerAttrib object that specifies whether to 
 * to enable conservative rasterization of primitives or not.
 */
CPT(RenderAttrib) RasterizerAttrib::
make(RasterizerAttrib::Mode mode) {
  RasterizerAttrib *attrib = new RasterizerAttrib(mode);
  return return_new(attrib);
}

/**
 * Returns a RenderAttrib that corresponds to whatever the standard default
 * properties for render attributes of this type ought to be.
 */
CPT(RenderAttrib) RasterizerAttrib::
make_default() {
  return return_new(new RasterizerAttrib(M_normal));
}

/**
 *
 */
void RasterizerAttrib::
output(std::ostream &out) const {
  out << get_type() << ":";
  switch (get_mode()) {
  case M_normal:
    out << "normal";
    break;

  case M_conservative:
    out << "conservative";
    break;
  }
}

/**
 * Intended to be overridden by derived RasterizerAttrib types to return a
 * unique number indicating whether this RasterizerAttrib is equivalent to the
 * other one.
 *
 * This should return 0 if the two RasterizerAttrib objects are equivalent, a
 * number less than zero if this one should be sorted before the other one,
 * and a number greater than zero otherwise.
 *
 * This will only be called with two RasterizerAttrib objects whose get_type()
 * functions return the same.
 */
int RasterizerAttrib::
compare_to_impl(const RenderAttrib *other) const {
  const RasterizerAttrib *ta = (const RasterizerAttrib *)other;

  return (int)_mode - (int)ta->_mode;
}

/**
 * Intended to be overridden by derived RenderAttrib types to return a unique
 * hash for these particular properties.  RenderAttribs that compare the same
 * with compare_to_impl(), above, should return the same hash; RenderAttribs
 * that compare differently should return a different hash.
 */
size_t RasterizerAttrib::
get_hash_impl() const {
  size_t hash = 0;
  hash = int_hash::add_hash(hash, (int)_mode);
  return hash;
}

/**
 * Intended to be overridden by derived RenderAttrib types to specify how two
 * consecutive RenderAttrib objects of the same type interact.
 *
 * This should return the result of applying the other RenderAttrib to a node
 * in the scene graph below this RenderAttrib, which was already applied.  In
 * most cases, the result is the same as the other RenderAttrib (that is, a
 * subsequent RenderAttrib completely replaces the preceding one).  On the
 * other hand, some kinds of RenderAttrib (for instance, ColorTransformAttrib)
 * might combine in meaningful ways.
 */
CPT(RenderAttrib) RasterizerAttrib::
compose_impl(const RenderAttrib *other) const {
  const RasterizerAttrib *ta = (const RasterizerAttrib *)other;
  return make(ta->get_mode());
}

/**
 * Tells the BamReader how to create objects of type RenderModeAttrib.
 */
void RasterizerAttrib::
register_with_read_factory() {
  BamReader::get_factory()->register_factory(get_class_type(), make_from_bam);
}

/**
 * Writes the contents of this object to the datagram for shipping out to a
 * Bam file.
 */
void RasterizerAttrib::
write_datagram(BamWriter *manager, Datagram &dg) {
  RenderAttrib::write_datagram(manager, dg);

  dg.add_int8(_mode);
}

/**
 * This function is called by the BamReader's factory when a new object of
 * type RasterizerAttrib is encountered in the Bam file.  It should create the
 * RasterizerAttrib and extract its information from the file.
 */
TypedWritable *RasterizerAttrib::
make_from_bam(const FactoryParams &params) {
  RasterizerAttrib *attrib = new RasterizerAttrib(M_normal);
  DatagramIterator scan;
  BamReader *manager;

  parse_params(params, scan, manager);
  attrib->fillin(scan, manager);

  return attrib;
}

/**
 * This internal function is called by make_from_bam to read in all of the
 * relevant data from the BamFile for the new RasterizerAttrib.
 */
void RasterizerAttrib::
fillin(DatagramIterator &scan, BamReader *manager) {
  RenderAttrib::fillin(scan, manager);

  _mode = (Mode)scan.get_int8();
}
